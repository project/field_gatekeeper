<?php

/**
 * @file
 * Module implementing a field gatekeeper pattern.
 *
 * The premise for this module is that CCK fields are entities that while
 * shared between node types that can be implemented in different modules,
 * still have common settings that needs a central gatekeeper.
 *
 * This concept is implemented in a quite simple way in this module. Here's
 * how to use it:
 *
 * Gatekeepers supply the shared setup of each field, by implementing
 * hook_field_gatekeeper().
 *
 * Field users use the fields by calling field_gatekeeper_update('modulename') in
 * their install or enable hooks, much like
 * drupal_install_schema. field_gatekeeper_update will then call hook_field_user
 * and hook_field_instances in the module, if they're
 * defined. hook_field_instances should return an array of field instance
 * settings, which will be created/updated. hook_field_user will simply be
 * called first and allows the module to do initial work, like ensuring a
 * content type exists.
 *
 * Fields are allowed to supply shared settings which will be used if no
 * gatekeeper would handle the field. This allows field users to still
 * function in the absence of a gatekeeper, but be careful with this feature.
 *
 * The module defines some drush commands to assist in creating the field
 * settings, and update the database.
 *
 * @todo Support for weights of non-field node items (taxonomy, menu, etc).
 */

/* --- HOOKS ---------------------------------------------------------------- */

/**
 * Implementation of hook_menu().
 */
function field_gatekeeper_menu() {
  $items = array();

  $items['admin/content/types/fields/lock/%'] = array(
    'title' => 'Lock field',
    'page callback' => 'field_gatekeeper_lock_unlock',
    'page arguments' => array(4, 5),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/content/types/fields/unlock/%'] = array(
    'title' => 'Unlock field',
    'page callback' => 'field_gatekeeper_lock_unlock',
    'page arguments' => array(4, 5),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/content/types/fields/update'] = array(
    'title' => 'Update fields',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('field_gatekeeper_form_confirm_update'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_menu_alter().
 */
function field_gatekeeper_menu_alter(&$items) {
  $items['admin/content/types/fields']['page callback'] = 'field_gatekeeper_fields_list';
}

/**
 * Implementation of hook_perm().
 */
function field_gatekeeper_perm() {
  return array('administer field_gatekeeper');
}

/**
 * Implementation of hook_theme().
 */
function field_gatekeeper_theme($existing, $type, $theme, $path) {
  return array(
    'field_gatekeeper_field_list' => array(
      'arguments' => array('rows'),
    ),
  );
}

/* --- CALLBACKS ------------------------------------------------------------ */

/**
 * Menu callback which lists all defined fields with handy links to update them,
 * lock them, and unlock them. Based on content_fields_list() from CCK.
 */
function field_gatekeeper_fields_list() {
  $fields = content_fields();
  $field_types = _content_field_types();

  module_load_include('inc', 'content', '/includes/content.crud');

  // Doing this manually, so we can see *who* defined a field.
  field_gatekeeper_ctools_plugin_api_include('field_gatekeeper', 'field_gatekeeper', 1, 1);
  foreach (module_implements('field_gatekeeper') as $module) {
    $module_fields = module_invoke($module, 'field_gatekeeper');
    foreach ($module_fields as $field) {
      $gatekeeper_fields[$field['field_name']] = $field;
      $gatekeeper[$field['field_name']] = $module;
    }
  }

  // sort fields by field name.
  ksort($fields);
  $rows = array();
  foreach ($fields as $field) {
    if ($field_types[$field['type']]) {
      $field_type = $field_types[$field['type']];
    } else {
      $field_type = array('label' => '<em>Unknown</em>');
    }
    $row = array();
    $row[] = $field['locked'] ? t('@field_name (Locked)', array('@field_name' => $field['field_name'])) : $field['field_name'];
    $row[] = t($field_type['label']);

    $row[] = isset($gatekeeper[$field['field_name']]) ? $gatekeeper[$field['field_name']] . '&nbsp;': '';
    
    $types = array();
    $result = db_query("SELECT nt.name, nt.type FROM {". content_instance_tablename() ."} nfi ".
    "LEFT JOIN {node_type} nt ON nt.type = nfi.type_name ".
    "WHERE nfi.field_name = '%s' ".
    // Keep disabled modules out of table.
    "AND nfi.widget_active = 1 ".
    "ORDER BY nt.name ASC", $field['field_name']);
    while ($type = db_fetch_array($result)) {
      $content_type = content_types($type['type']);
      $types[] = l($type['name'], 'admin/content/node-type/'. $content_type['url_str'] .'/fields');
    }
    $row[] = implode(', ', $types);

    // show status
    if (!isset($gatekeeper_fields[$field['field_name']])) {
      $row[] = t('Only in DB');
    }
    else {
      $clean = TRUE;

      $instances = field_gatekeeper_get_field_instances();
      if (!empty($instances)) {
        foreach ($instances as $instance) {
          if ($instance['field_name'] != $field['field_name']) {
            continue;
          }

          // get the field instance for this content type from the database
          $fields_db = content_field_instance_read(array('field_name' => $field['field_name'], 'type_name' => $instance['type_name']));
          $field_db = array_shift($fields_db);

          // build a complete field instance from the gatekeeper
          $field_gatekeeper = $gatekeeper_fields[$field['field_name']] + $instance;

          // prepare the two instances for comparison...
          $gatekeeper_clean = field_gatekeeper_instance_clean($field_gatekeeper);
          $db_clean = field_gatekeeper_instance_clean($field_db);
          unset($gatekeeper_clean['type_name'], $db_clean['type_name']);

          // ...and compare them
          $rdiff = _field_gatekeeper_array_rdiff($gatekeeper_clean, $db_clean);
          if (!empty($rdiff)) {
            $clean = FALSE;
          }
        }
      }

      $row[] = $clean ? t('Clean') : t('Dirty');
    }

    // build links for operations list
    if ($field['locked']) {
      $operations = l(t('Unlock'), 'admin/content/types/fields/unlock/'. $field['field_name'], array('query' => drupal_get_destination()));
    }
    else {
      $operations = l(t('Lock'), 'admin/content/types/fields/lock/'. $field['field_name'], array('query' => drupal_get_destination()));
    }
    $row[] = $operations;
    // TODO: update if out of sync with module-defined field.

    $rows[] = array('data' => $row, 'class' => $field['locked'] ? 'menu-disabled' : '');
  }

  return theme('field_gatekeeper_field_list', $rows);
}


/**
 * Lock/unlock callback.
 */
function field_gatekeeper_lock_unlock($op, $field_name) {
  module_load_include('inc', 'content', '/includes/content.crud');

  $instances = content_field_instance_read(array('field_name' => $field_name));

  foreach ($instances as $instance) {
    $instance['locked'] = $op == 'lock' ? 1 : 0;
    content_field_instance_update($instance);
  }
  if ($op == 'lock') {
    drupal_set_message(t('The field %field has been locked.', array('%field' => $field_name)));
  }
  else {
    drupal_set_message(t('The field %field has been unlocked.', array('%field' => $field_name)));
  }
  drupal_goto();
}

/* --- FORMS ---------------------------------------------------------------- */

/**
 * Confirmation form for updating all fields.
 */
function field_gatekeeper_form_confirm_update($form_state) {
  return confirm_form(
    array(),
    t('Are you sure you want to update all fields?'),
    'admin/content/types/fields',
    t('This will reset all module defined fields.')
  );
}

/**
 * Submit handler for the update fields confirmation form.
 */
function field_gatekeeper_form_confirm_update_submit($form, &$form_state) {
  field_gatekeeper_update_all();
  drupal_set_message(t('All fields have been updated.'));
  $form_state['redirect'] = 'admin/content/types/fields';
}

/* --- API ------------------------------------------------------------------ */

/**
 * Setup a field.
 *
 * Gatekeep'ed field using modules might use this to add gatekeep'ed
 * fields to content types, however, the recommended usage is through
 * field_gatekeeper_update which will call this for every field
 * returned by hook_field_instances.
 *
 * This function will query the gatekeepers for the settings of the
 * field, add the supplied instance settings and create or update the
 * field on the specified content type.
 *
 * The field user may supply common settings, which will be used if
 * there's no gatekeepers that knows the field. This allows users to
 * work without gatekeepers, but should be used with care as it
 * provides no protection against multiple users supplying conflicting
 * settings.
 *
 * @param string $content_type The name of the content type to add the field to.
 * @param array $field The field settings.
 * @return boolean FALSE on error, TRUE otherwise,
 */
function field_gatekeeper($field, $content_type = NULL) {
  static $field_setup = NULL;
  if (!$field['field_name']) {
    return FALSE;
  }
  if (!$content_type) {
    $content_type = $field['type_name'];
  }
  if (!$content_type) {
    return FALSE;
  }
  module_load_include('inc', 'content', '/includes/content.crud');
  $field_name = $field['field_name'];

  if (is_null($field_setup)) {
    $field_setup = field_gatekeeper_get_field_gatekeeper();
  }

  if (isset($field_setup[$field_name]) and isset($field_setup[$field_name]['type'])) {
    // Clean field setup for instance setup.
    $field = field_gatekeeper_base_clean($field_setup[$field_name]) + $field;
  }

  // Sanity check field for display settings, widget and common setup.
  if (!isset($field['type']) or !isset($field['display_settings']) or
      !isset($field['widget'])) {
    return FALSE;
  }

  // Lock from modification.
  $field['locked'] = '1';
  $existing = content_field_instance_read(array('type_name' => $content_type,
                                                'field_name' => $field['field_name']));
  if (sizeof($existing) > 0) {
    content_field_instance_update($field, FALSE);
  } else {
    // content_field_instance_create does odd things and set these to
    // a previous instances values, if we don't do this.
    $field['weight'] = $field['widget']['weight'];
    $field['label'] = $field['widget']['label'];
    $field['description'] = $field['widget']['description'];
    content_field_instance_create($field, FALSE);
  }
  // Check whether the field exists now.
  if (content_field_instance_read(array('type_name' => $content_type,
                                        'field_name' => $field['field_name']))) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Update fieldgroups.
 */
function field_gatekeeper_group($group) {
  fieldgroup_save_group($group['type_name'], $group);
  foreach ($group['fields'] as $field) {
    fieldgroup_update_fields(array(
        'field_name' => $field,
        'group' => $group['group_name'],
        'type_name' => $group['type_name'],
      ));
  }
}

function field_gatekeeper_update_all() {
  field_gatekeeper_ctools_plugin_api_include('field_gatekeeper', 'field_instances', 1, 1);
  field_gatekeeper_ctools_plugin_api_include('field_gatekeeper', 'field_groups', 1, 1);
  $modules = module_implements('field_instances');
  $modules = array_unique(array_merge($modules, module_implements('field_groups')));

  foreach ($modules as $module) {
    field_gatekeeper_update($module);
  }
}


function field_gatekeeper_update($module) {
  static $last_enabled_modules = NULL;
  $enabled_modules = module_list(TRUE, FALSE);
  if ($last_enabled_modules && ($last_enabled_modules != $enabled_modules)) {
    // Clear caches if new modules enabled.
    ctools_static_reset('ctools_plugin_api_info');
    ctools_static_reset('field_gatekeeper_ctools_plugin_api_include');
  }
  $last_enabled_modules = $enabled_modules;
  
  if (module_hook($module, 'field_user')) {
    $res = module_invoke($module, 'field_user');
  }

  $res2 = TRUE;
  $fields = field_gatekeeper_get_field_instances($module);
  foreach ($fields as $field) {
    // Bitwise operator, as we don't want to shortcut.
    $res2 = $res2 & field_gatekeeper($field);
  }

  if (module_exists('fieldgroup')) {
    $groups = field_gatekeeper_get_field_groups($module);
    foreach ($groups as $group) {
      // The fieldgroup module doesn't return anything we can use.
      field_gatekeeper_group($group);
    }
  }
  content_clear_type_cache(TRUE);
  menu_rebuild();

  if (is_null($res)) {
    $res = $res2;
  } else {
    $res = $res && $res2;
  }
  return $res;
}

function field_gatekeeper_collect($hook) {
  $items = array();
  foreach (module_implements($hook) as $module) {
    $items = array_merge($items, module_invoke($module, $hook));
  }
  return $items;
}

/**
 * ctools_plugin_api_include caches the APIs it's already loaded, but
 * we need to be able to redo an API, in the case a new module has
 * been enabled since the last call to field_gatekeeper. This might
 * happen when called from drush.
 */
function field_gatekeeper_ctools_plugin_api_include($owner, $api, $minimum_version, $current_version) {
  ctools_include('plugins');
  $already_done = &ctools_static(__FUNCTION__, array());

  $info = ctools_plugin_api_info($owner, $api, $minimum_version, $current_version);
  if (!isset($already_done[$owner][$api])) {
    foreach ($info as $module => $plugin_info) {
      if (!isset($plugin_info['file'])) {
        $plugin_info['file'] = "$module.$api.inc";
      }
      if (file_exists("./$plugin_info[path]/$plugin_info[file]")) {
        $plugin_info[$module]['included'] = TRUE;
        require_once "./$plugin_info[path]/$plugin_info[file]";
      }
      $info[$module] = $plugin_info;
    }
    $already_done[$owner][$api] = TRUE;
  }

  return $info;
}

function field_gatekeeper_get_field_instances($module = NULL) {
  field_gatekeeper_ctools_plugin_api_include('field_gatekeeper', 'field_instances', 1, 1);
  if ($module) {
    if (module_hook($module, 'field_instances')) {
      $module_fields = module_invoke($module, 'field_instances');
    } else {
      return array();
    }
  } else {
    $module_fields = field_gatekeeper_collect('field_instances');
  }
  $fields = array();
  // Normalise field array. In reality we don't have a key, but we
  // need one for identification for exports. So we concat type and
  // field name, and as a bonus, this ensures that conflicts are
  // handled according to module weights. Also, this approach means
  // that implementers doesn't *have* to return a keyed array.
  foreach ($module_fields as $field) {
    $field['name'] = $field['type_name'] . '-' . $field['field_name'];
    $fields[$field['name']] = $field;
  }

  return $fields;
}

function field_gatekeeper_get_field_groups($module = NULL) {
  field_gatekeeper_ctools_plugin_api_include('field_gatekeeper', 'field_groups', 1, 1);
  if ($module) {
    if (module_hook($module, 'field_groups')) {
      $module_groups = module_invoke($module, 'field_groups');
    } else {
      return array();
    }
  } else {
    $module_groups = field_gatekeeper_collect('field_groups');
  }

  $groups = array();
  // Normalise group array. Same ideas as for field_instances.
  foreach ($module_groups as $group) {
    $group['name'] = $group['type_name'] . '-' . $group['group_name'];
    $groups[$group['name']] = $group;
  }
  return $groups;
}

function field_gatekeeper_get_field_gatekeeper() {
  field_gatekeeper_ctools_plugin_api_include('field_gatekeeper', 'field_gatekeeper', 1, 1);
  return field_gatekeeper_collect('field_gatekeeper');
}

function field_gatekeeper_instance_clean($field) {
  $tmp = array();
  $tmp['type_name'] = $field['type_name'];
  $tmp['field_name'] = $field['field_name'];
  $tmp['name'] = $field['type_name'] . '-' . $field['field_name'];
  $tmp['widget'] = $field['widget'];
  $tmp['widget_active'] = $field['widget_active'];
  $tmp['display_settings'] = $field['display_settings'];
  return $tmp;
}

function field_gatekeeper_base_clean($field) {
  unset(
    $field['type_name'],
    $field['widget'],
    $field['widget_active'],
    $field['display_settings'], 
    $field['db_storage'] /* CCK manages storage depending on whether
                          * the field is multiple, or there's more
                          * than one instance. We don't try to tell
                          * it.
                          */
  );
  return $field;
}

function field_gatekeeper_check_fields() {
  module_load_include('inc', 'content', '/includes/content.crud');
  $fields = field_gatekeeper_get_field_gatekeeper();
  $res = array();
  foreach ($fields as $field) {
    if ($instance = content_field_instance_read(array( 'field_name' => $field['field_name']))) {
      if (array_diff(field_gatekeeper_base_clean($instance[0]), field_gatekeeper_base_clean($field))) {
        drush_print_r($diff);
        $res[] = $field['field_name'];
      }
    } else {
      $res[] = $field['field_name'];
    }
  }
  return $res;
}

/* --- UTILITY -------------------------------------------------------------- */

/**
 * Compare two arrays recursively.
 *
 * @param array $a
 *   The reference array.
 * @param array $b
 *   The array to compare against the reference array.
 * @return array
 *   An array containing all the differences between the array and the
 *   reference array.
 */
function _field_gatekeeper_array_rdiff($a, $b) {
  $diff = array_diff_assoc($a, $b);
  foreach ($a as $key => $value) {
    if (is_array($a[$key]) && is_array($b[$key])) {
      $rdiff = _field_gatekeeper_array_rdiff($a[$key], $b[$key]);
      if (!empty($rdiff)) {
        $diff[$key] = $rdiff;
      }
    }
  }
  return $diff;
}

/* --- THEME ---------------------------------------------------------------- */

/**
 * Theme the content type overview page.
 */
function theme_field_gatekeeper_field_list($rows) {
  $output = '';

  // render the main overview table
  $header = array(t('Field name'), t('Field type'), t('Gatekeeper'), t('Used in'), t('Status'), t('Operations'));
  if (empty($rows)) {
    $output .= t('No fields have been defined for any content type yet.');
  }
  else {
    $output .= theme('table', $header, $rows);
  }

  // add some help text...
  $items = array(
    t('<strong>Only in DB:</strong> The field is only defined in the database.'),
    t('<strong>Clean:</strong> No changes have been made to this field.'),
    t('<strong>Dirty:</strong> This field is different from its gatekeeper definition.'),
  );
  $output .= theme('item_list', $items);

  // ...and a link to update the fields
  $output .= '<p>'. l(t('Update all fields'), 'admin/content/types/fields/update') .'</p>';

  return $output;
}
