<?php

/**
 * @file
 * Drush commands for the Field Gatekeeper module.
 */

/* --- HOOKS ---------------------------------------------------------------- */

/**
 * Implementation of hook_drush_command().
 *
 * We use Drush's automagic naming of callback functions, where command names
 * are mapped to callbacks on the form:
 *
 *   drush_<module name>_<command>
 *
 * In this case, the Drush command 'fg comp' is mapped to the callback function
 *
 *   drush_field_gatekeeper_fg_comp()
 */
function field_gatekeeper_drush_command() {
  $items['fg comp'] = array(
    'description' => 'Compare field instances to catch modifications.',
    'arguments' => array(
      'name' => 'The name of the field whose instances you wish to compare. Mandatory.',
    ),
    'examples' => array(
      'fg comp field_teaser' => 'Compare all instances of field_teaser.',
    ),
    'aliases' => array('fgc'),
    'drupal dependencies' => array('content'),
  );

  $items['fg dump'] = array(
    'description' => 'Dump settings for a specific field.',
    'arguments' => array(
      'fields' => 'The names of one or more fields you wish to dump. Mandatory.',
    ),
    'options' => array(
      '-f, --basic' => 'Dump basic field settings only, ignore display and widget settings.',
      '-t, --type' => 'Dump field instance settings for this node type only.',
    ),
    'examples' => array(
      'fg dump field_teaser' => 'Dump all field_teaser instance settings.',
      'fg dump -f field_teaser' => 'As above, but ignore display and widget settings.',
      'fg dump -t story field_teaser' => 'Dump field_teaser instance settings from the story content type.',
    ),
    'aliases' => array('fgd'),
    'drupal dependencies' => array('content', 'field_gatekeeper'),
  );

  $items['fg lock'] = array(
    'description' => 'Lock one or more fields.',
    'arguments' => array(
      'fields' => 'The names of one or more fields you wish to lock. Mandatory.',
    ),
    'examples' => array(
      'fg lock field_teaser' => 'Lock the field_teaser field.',
    ),
    'drupal dependencies' => array('content'),
  );

  $items['fg unlock'] = array(
    'description' => 'Unlock one or more fields.',
    'arguments' => array(
      'fields' => 'The names of one or more fields you wish to unlock. Mandatory.',
    ),
    'examples' => array(
      'fg unlock field_teaser' => 'Unlock the field_teaser field.',
    ),
    'drupal dependencies' => array('content'),
  );

  $items['fg status'] = array(
    'description' => 'Show the status of fields from all enabled modules.',
    'examples' => array(
      'fg status' => 'Show field status.',
    ),
    'drupal dependencies' => array('content'),
  );

  $items['fg update'] = array(
    'description' => 'Unlock one or more fields.',
    'arguments' => array(
      'fields' => 'The names of one or more fields you wish to update. Mandatory.',
    ),
    'examples' => array(
      'fg update field_teaser' => 'Update the field_teaser field.',
    ),
    'drupal dependencies' => array('content'),
  );

  // TODO: sanity check function that queries all gatekeepers and ensures that they don't disagree on any field.
  // TODO: Check the gatekeepers against the database

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function field_gatekeeper_drush_help($section) {
  switch ($section) {
    case 'drush:fg comp':
      return dt('Extended help text goes here.');
    case 'drush:fg dump':
      return dt('Extended help text goes here.');
    case 'drush:fg lock':
      return dt('Extended help text goes here.');
    case 'drush:fg unlock':
      return dt('Extended help text goes here.');
    case 'drush:fg status':
      return dt('Extended help text goes here.');
    case 'drush:fg update':
      return dt('Extended help text goes here.');
  }
}

/* --- CALLBACKS ------------------------------------------------------------ */

/**
 * Callback for the fg comp command.
 */
// TODO: add option for using 'type_name' as well.
function drush_field_gatekeeper_fg_comp() {
  $args = func_get_args();
  $name = array_shift($args);

  if (empty($name)) {
    return drush_set_error(dt('Field name argument is missing.'));
  }

  module_load_include('inc', 'content', '/includes/content.crud');

  // load all instances of the field
  $options = array('field_name' => $name);
  $instances = content_field_instance_read($options);

  if (empty($instances)) {
    drush_log(dt('No instances of "@field" available.', array('@field' => $name)), 'error');
  }
  else {
    if (!function_exists('_field_gatekeeper_array_rdiff')) {
      module_load_include('module', 'field_gatekeeper', 'field_gatekeeper');
    }

    // use the first instance as a reference when comparing the field instances
    $reference = array_shift($instances);
    $diffs = array();

    if (!empty($instances)) {
      // compare each remaining field instance to the reference field
      foreach ($instances as $instance) {
        $key = $instance['type_name'];

        // compare the fields and get rid of the type name since it differs
        // from field instance to field instance
        $diff = _field_gatekeeper_array_rdiff($instance, $reference);
        unset($diff['type_name']);

        if (!empty($diff)) {
          $diffs[$key] = $diff;
        }
      }
    }

    if (!empty($diffs)) {
      drush_print(dt('Using instance from "@type" as reference for comparing "@field".', array('@type' => $reference['type_name'], '@field' => $name)));
      foreach ($diffs as $type => $diff) {
        drush_print("Content type: ". $type);
        drush_print(var_export($diff));
      }
      drush_log(dt('A total of @count field instance(s) have been modified.', array('@count' => count($diffs))), 'failed');
    }
    else {
      drush_log(dt('No field instances have been modified.'), 'ok');
    }
  }
}

/**
 * Callback for the fg dump command.
 */
function drush_field_gatekeeper_fg_dump() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error(dt('Field name argument is missing.'));
  }

  module_load_include('inc', 'content', '/includes/content.crud');

  if (!function_exists('field_gatekeeper')) {
    module_load_include('module', 'field_gatekeeper', 'field_gatekeeper');
  }

  $content_type = drush_get_option(array('t', 'type'), FALSE);
  $field_only = drush_get_option(array('f', 'basic'), FALSE);

  foreach ($args as $field_name) {
    if ($content_type) {
      $instances = content_field_instance_read(array('field_name' => $field_name, 'type_name' => $content_type));
    }
    else {
      $instances = content_field_instance_read(array('field_name' => $field_name));
    }

    if (!$instances) {
      drush_set_error(dt('Unknown field @field.', array('@field' => $field_name)));
    }
    else {
      $dump = '';
      foreach ($instances as $instance) {
        $instance['locked'] = 1;

        if ($field_only) {
          $instance = field_gatekeeper_base_clean($instance);
          $dump .= "'". $instance['field_name'] ."' => ";
        }
        elseif ($content_type) {
          $instance = field_gatekeeper_instance_clean($instance);
          $dump .= "'". $instance['name'] ."' => ";
        }

        $dump .= var_export($instance, TRUE);
        $dump .= ",\n";
      }

      // indent the exported data to make it more readable
      $dump = preg_replace('/^/m', '    ', $dump);

      // clean up the dump a bit by removing unwanted white space
      $dump = preg_replace('/array\s+\(/', 'array(', $dump);
      $dump = preg_replace('/=>\s+array\(/', '=> array(', $dump);

      drush_print($dump);
    }
  }
}

/**
 * Callback for the fg lock command.
 */
function drush_field_gatekeeper_fg_lock() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error(dt('Field name argument is missing.'));
  }

  module_load_include('inc', 'content', '/includes/content.crud');

  foreach ($args as $field_name) {
    $instances = content_field_instance_read(array('field_name' => $field_name));

    if (!$instances) {
      drush_set_error(dt('Unknown field @field.', array('@field' => $field_name)));
    }
    else {
      foreach ($instances as $instance) {
        $instance['locked'] = TRUE;
        content_field_instance_update($instance);

        drush_log(dt('Field @field on content type @type locked.', array('@field' => $instance['field_name'], '@type' => $instance['type_name'])), 'ok');
      }
    }
  }
}

/**
 * Callback for the fg unlock command.
 */
function drush_field_gatekeeper_fg_unlock() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error(dt('Field name argument is missing.'));
  }

  module_load_include('inc', 'content', '/includes/content.crud');

  foreach ($args as $field_name) {
    $instances = content_field_instance_read(array('field_name' => $field_name));

    if (!$instances) {
      drush_set_error(dt('Unknown field @field.', array('@field' => $field_name)));
    }
    else {
      foreach ($instances as $instance) {
        $instance['locked'] = FALSE;
        content_field_instance_update($instance);

        drush_log(dt('Field @field on content type @type unlocked.', array('@field' => $instance['field_name'], '@type' => $instance['type_name'])), 'ok');
      }
    }
  }
}

/**
 * Callback for the fg status command.
 */
function drush_field_gatekeeper_fg_status() {
  if (!function_exists('field_gatekeeper')) {
    module_load_include('module', 'field_gatekeeper', 'field_gatekeeper');
  }

  $diffs = field_gatekeeper_check_fields();
  if ($diffs) {
    drush_log(dt('The following fields are dirty: @names', array('@names' => join(', ', $diffs))), 'error');
  }
  else {
    drush_log(dt('All fields are clean.'), 'ok');
  }
}

/**
 * Callback for the fg update command.
 */
function drush_field_gatekeeper_fg_update() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error(dt('Module name argument is missing.'));
  }

  if (!function_exists('field_gatekeeper')) {
    module_load_include('module', 'field_gatekeeper', 'field_gatekeeper');
  }

  foreach ($args as $module) {
    drush_log(dt('Updating fields from @module.', array('@module' => $module)), 'ok');
    field_gatekeeper_update($module);
  }
}
