
Introduction
============

CCK with Views/CTools exportable flavour...

Many has wished for Views default_views functionality for Content
Construction Kit, however, the nature of how CCK works makes this
difficult for two reasons:

1. Changing field settings might potentially destroy data, or cause
CCK to move large amounts of data between tables.

2. Each field is really divided in two: Shared database settings
(column types, sizes, multiple, required, etc), and instance specific
settings (formatters).

Field Gatekeeper attempts to deal with these issues by:

1. Only updating fields when asked to, from drush commands, through
it's admin page, or by a module specifically calling it from an
install or update function.

2. Splitting the field setup between 'gatekeepers' which decide on the
global settings, and instance users (content type implementing
modules).

We have been using this model for a number of projects, and it's
proved itself as a useful way to deal with CCK on a number of sites.

Usage
=====

Defining the hook_field_gatekeeper and hook_field_instances should be
familiar to CTools users, as Field Gatekeeper uses regular CTools
exportables as a base. There's Drush commands to dump individual
fields, but a much easier option is to use Drush EM
(http://drupal.org/project/drush_em) to dump directly into inc
files. The build in Drush commands is likely to be deprecated in
favour of Drush EM in the future.

There's also some documentation in the header of the module file.
